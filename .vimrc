"===========================================================================================
"                             Mappings, and settings
set nocompatible                             
filetype on "for the plug in, and lets vim know which file detection type is, for the ctag plugin
if has("gui_running")
    " C-Space seems to work under gVim on both Linux and win32
    inoremap <C-Space> <C-x><C-o>
    else " no gui
    if has("unix")
      inoremap <Nul> <C-x><C-o>
    else
    endif
endif

call pathogen#helptags()
call pathogen#runtime_append_all_bundles()

set nobackup
set noswapfile

set pastetoggle=<F2>

set list
set listchars=tab:>.,trail:.,extends:#,nbsp:.

nmap <silent> <leader>ev :e $MYVIMRC<CR>
nmap <silent> <leader>sv :so $MYVIMRC<CR>

let mapleader=","
nnoremap <Leader>/ :nohlsearch<CR>
nnoremap <Leader><TAB> :LustyJuggler<CR>
"saving pinky pain
nnoremap : ;
nnoremap ; :

set hidden

"Changing tabs yo
"I needed to use the gconf-editor to manipulate the fullscreen <F11> and menu
"<F10>
noremap <silent> <F9> :tabnext<CR>
noremap <silent> <F10> :tabprevious<CR>
noremap <silent> <F11> :tabnew<CR>
noremap <silent> <F12> :w  <CR> :tabclose <CR> "for safety, i dont know if tabclose saves on close, also won't work on new tabs, unfortunately, need to give a name to those

set backspace=indent,eol,start

"Just keep swapping, just keep swapping
noremap <C-F12> :Invbg<CR>
"===========================================================================================
"                                   Terminal settings and HUD
set backup "----------------------
set backupdir=~/.vim/backup "    |--- three lines that create a back up dir for all the backup files and tmp
set directory=~/.vim/tmp "--------
set autochdir "the working directory is always the same as the file, for the terminal
set relativenumber 
set cursorline "current line cursor is on, is highlighted
set scrolloff=10 "so, and setting where the line should start, greater than 7 is more to the middle, eg
set ignorecase "ignores case while searching
set smartcase "go AI :D
set incsearch "goes to the next search word as we type
set expandtab "no spaces for me
set smarttab "smart spaces for me
set autoindent "retains indenting from prev line
set smartindent "automatically indents in some cases
set shiftwidth=4 "a little smaller than 8
set softtabstop=4 "it's a magic number
if version >= 700 "spell checking, although not spell checking by defulat
   set spl=en spell
   set nospell
endif
set mouse=a "mouse can be nice 
set showmatch "show's brackets around in matching ones
set hlsearch "highlights search objects in search
set wildmenu "in command line hit <TAB> to show likely completions
set showcmd  "showing what im typing isn't such a bad idea
set copyindent

"terminal font set seprately
if has('gui_running')
    set guifont=inconsolata\ 12
    set guioptions-=m  "remove menu bar
    set guioptions-=T  "remove toolbar
    set guioptions-=r  "remove right-hand scroll bar 
endif
"i don't like noises
set noerrorbells
set visualbell
set t_vb=

syntax enable "enable syntax for files
"===========================================================================================
"                            Color scheme
set background=dark
colorscheme zenburn "the custom one in ~/.vim/colors
"===========================================================================================
"                               Plugins
filetype plugin on
filetype indent on
filetype plugin indent on
autocmd filetype python set expandtab

set omnifunc=syntaxcomplete#Complete " C-n and C-P for the moving down the pop ups, whilst still in time moving the actual code
"For taglist:
"" Taglist plugin mapping

"===========================================================================================
"                        Helper Functions
nnoremap j gj
nnoremap k gk

"Easy window navigation
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

"I made the change
map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>
